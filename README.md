# NI-MVI Semestral work

This is a semestral work from NI-MVI on topic *Data augmentation using deep learning methods*.

Dataset used: [Kaggle - Sberbank Russian Housing Market](https://www.kaggle.com/competitions/sberbank-russian-housing-market/data)

## Assignment

The goal is to augment the dataset using DL techniques in order to obtain better prediction results. The improvement should be assessed by training a regression model on both the raw dataset and the augmented dataset and comparing the prediction errors.  

## Repo structure

* [src](https://gitlab.fit.cvut.cz/jenicja1/mvi-sp/-/tree/master/src) - code base
    * [mvi](https://gitlab.fit.cvut.cz/jenicja1/mvi-sp/-/tree/master/src/mvi) - package with model definition & utilities
    * [./](https://gitlab.fit.cvut.cz/jenicja1/mvi-sp/-/tree/master/src/) - IPYNB notebooks
* [quiz](https://gitlab.fit.cvut.cz/jenicja1/mvi-sp/-/tree/master/quiz) - quiz screenshot
* [text](https://gitlab.fit.cvut.cz/jenicja1/mvi-sp/-/tree/master/text) - everything report-related  


## DL models

* [VAE](https://arxiv.org/abs/1312.6114) 
* [CTGAN](https://proceedings.neurips.cc/paper/2019/file/254ed7d2de3b23ab10936522dd547b78-Paper.pdf)




