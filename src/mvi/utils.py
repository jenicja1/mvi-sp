"""
Collection of general utility functions that can be used accross notebooks
"""

import os
import sys
import time
import datetime
import numpy as np
import pandas as pd
from scipy import stats
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt
import seaborn as sns

from typing import Callable, Any, Optional, Tuple


def list_data_files(path=".", format_suffix="csv") -> None:
    """
    Lists all data files in given directory
    """
    files_all = os.listdir(path)
    files_suffix = [p for p in files_all if str(p).endswith(f".{format_suffix}")]
    print("Available files:")
    for p in files_suffix:
        print(f"  {p}")


def measure_time(f) -> Callable[..., Tuple[Any, float]]:
    """
    Decorates the function with time measuring

    The time is returned as the second parameter
    """
    def wrapper(*args, **kwargs) -> tuple[Any, float]:
        # start the clock
        time_init = time.perf_counter()
        # call the function
        res = f(*args, **kwargs)
        # return the function result and the time of execution
        time_run = time.perf_counter() - time_init
        return res, time_run

    return wrapper


class DateEncoder:
    """
    Handles encoding of dates into integers (and decoding them back)
    """
    def __init__(self) -> None:
        self._offset: Optional[datetime.date] = None
        
    def encode(self, data: pd.Series) -> pd.Series:
        """Encodes datetime series into integers"""
        data_date = data.apply(lambda x: x.date())
        self._offset = data_date.min()
        
        return data_date.apply(lambda x: (x - self._offset).days)
        
    def decode(self, data: pd.Series) -> pd.Series:
        """Decodes series of integers back into datetime format"""
        if self._offset is None:
            raise AttributeError("`decode` cannot be called before `encode`")
            
        return data.apply(lambda x: self._offset + datetime.timedelta(days=x))


def mkdir_safe(dir_path: str) -> bool:
    """Creates new directory if it does not exist"""
    if not os.path.exists(dir_path):
        os.mkdir(dir_path)
        return True
    return False


def _print_schema_mismatch(a: pd.DataFrame, b: pd.DataFrame, /) -> None:
    """Prints where schemas of two DFs differ"""
    print("schema mismatch:")
    cols_a, cols_b = map(set, (a, b))
    if cols_a != cols_b:
        print(f"Column names mismatch. diff: {cols_a - cols_b} x {cols_b - cols_a}")
        return
    mask_mismatch = a.dtypes != b.dtypes
    print(f"{a.dtypes.loc[mask_mismatch]}")
    print("-----------------")
    print(f"{b.dtypes.loc[mask_mismatch]}")
    

def check_schema(*dfs: pd.DataFrame, verbose: bool = False) -> None:
    """Ensures all DFs have the same schema"""
    if len(dfs) <= 1:
        return
    
    df_ref, dfs_other = dfs[0], dfs[1:]
    for i, df in enumerate(dfs_other):
        # compare two DFs
        if not df_ref.dtypes.equals(df.dtypes):
            # show results
            if verbose:
                _print_schema_mismatch(df_ref, df)
            raise ValueError(f"schema mismatch at positions: 0 & {i + 1}.")
    
    if verbose:
        print("All schemas match.")


def remove_outliers_zscore(df: pd.DataFrame, cols_ignore=None, zscore: int = 3) -> pd.DataFrame:
    """Removes all rows that act as outliers in 1+ columns"""
    if cols_ignore is None:
        cols_ignore = []
    cols_ignore = [col for col in cols_ignore if col in df.columns]
    df_drop = df.drop(cols_ignore, axis=1)
    mask_ok = np.abs(stats.zscore(df_drop) < zscore).all(axis=1)
    
    n_total, n_removed = df.shape[0], sum(~mask_ok)
    print(f"[zscore] Number of removed samples: {n_removed} / {n_total} ~ {n_removed / n_total:.2%}")
    
    return df.drop(mask_ok[~mask_ok].index)


def remove_outliers_pca_zscore(df: pd.DataFrame, n_components: int, cols_ignore=None, zscore: int = 3) -> pd.DataFrame:
    """Removes all rows that act as outliers in 1+ of DF's principal components"""
    if cols_ignore is None:
        cols_ignore = []
    cols_ignore = [col for col in cols_ignore if col in df.columns]
    df_drop = df.drop(cols_ignore, axis=1)
    
    components = PCA(n_components=n_components).fit_transform(df_drop)
    df_components = pd.DataFrame(data=components)
    mask_ok = np.abs(stats.zscore(df_components) < zscore).all(axis=1)
                                       
    n_total, n_removed = df.shape[0], sum(~mask_ok)
    print(f"[zscore] Number of removed samples: {n_removed} / {n_total} ~ {n_removed / n_total:.2%}")
    
    return df.drop(mask_ok[~mask_ok].index)


def get_pca(df: pd.DataFrame, n_components: int = 2, plot_first_two: bool = True) -> pd.DataFrame:
    """Gets principal components that represent passed features dataframe"""
    components = PCA(n_components=n_components).fit_transform(df)
    if plot_first_two and components.shape[1] >= 2:
        c_first, c_second = components[:, 0], components[:, 1]
        _, axs = plt.subplots(1, 1, figsize=(4, 4))
        sns.scatterplot(ax=axs, x=c_first, y=c_second)
    
    return components


def replace_with_flag_column(df: pd.DataFrame, cols_dict: dict) -> pd.DataFrame:
    """
    Replaces all columns in provided dictionary with a simplified flag column of the same name.
    
    For each dict key (column), assigns 1 to all rows equal to the dict value, 0 otherwise
    """
    assign_dict = {
        col_name: lambda x: (x.loc[:, col_name] == positive_flag_val).astype(int)
        for col_name, positive_flag_val
        in cols_dict.items()
    }
    df_flags = df.assign(**assign_dict)
    
    rename_dict = {
        col_name: f"{col_name}_is_{positive_flag_val}".lower()
        for col_name, positive_flag_val
        in cols_dict.items() 
    }
    df_renamed = df_flags.rename(rename_dict, axis=1)
    
    return df_renamed