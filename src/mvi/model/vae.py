from typing import Any, Callable, Tuple, Optional, Iterable
import pandas as pd

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import Linear
import torch.utils
import torch.distributions
from torch.nn.functional import normalize
import numpy as np
import torch
from torch.optim import Adam
from torch.utils.data import TensorDataset 


DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'


def _get_tensor_data(data_np: np.ndarray) -> Iterable[torch.Tensor]:
    tensor_data = TensorDataset(
        torch
        .from_numpy(data_np.astype("float32"))
        .to(DEVICE)
    )
    return [x[0] for x in tensor_data]


def _kl_div(sigma, mu) -> int:
    """Computes KL divergence"""
    return (sigma**2 + mu**2 - torch.log(sigma) - 1/2).sum()


class VariationalEncoder(nn.Module):
    """
    Represent a variational encoder
    """
    def __init__(
        self, 
        dim_latent: int, 
        dim_data: int,
        dim_hidden: int, 
        activation_fn: Callable = lambda x: x,
    ) -> None:
        super().__init__()
        self._l1 = Linear(dim_data, dim_hidden)
        self._l_mu = Linear(dim_hidden, dim_latent)
        self._l_sigma = Linear(dim_hidden, dim_latent)
        self.activation_fn = activation_fn

        self.kl = 0
        self._distr = torch.distributions.Normal(0, 1)


    def forward(self, x) -> Tuple[Any, torch.Tensor]:
        x = normalize(x, dim=0)
        x = self.activation_fn(self._l1(x))
        mu = self._l_mu(x)
        sigma = torch.exp(self._l_sigma(x))

        self.kl = _kl_div(sigma, mu)

        z = mu + sigma * self._distr.sample(mu.shape)
        return z


class Decoder(nn.Module):
    """
    Represent a decoder
    """
    def __init__(
        self, 
        dim_latent: int, 
        dim_data: int,
        dim_hidden: int = 512, 
        activation_fn: Callable = lambda x: x,
    ) -> None:
        super().__init__()
        # initialize the structure: Linear + activation function per each layer
        self.l1 = Linear(dim_latent, dim_hidden)
        self.l2 = Linear(dim_hidden, dim_data)

        self.activation_fn = activation_fn


    def forward(self, x) -> Tuple[Any, torch.Tensor]:
        x = self.activation_fn(self.l1(x))
        return self.activation_fn(self.l2(x))


class VAE(nn.Module):
    """
    Variational autoencoder
    """
    def __init__(
        self, 
        dim_latent: int, 
        dim_hidden: Optional[int] = None, 
        n_epochs: int = 5,
        verbose: bool = False,
    ) -> None:
        super().__init__()
        self.dim_latent = dim_latent
        self.dim_hidden = dim_latent * 2 if dim_hidden is None else dim_hidden
        self.n_epochs = n_epochs
        self.verbose = verbose
        self.encoder = None
        self.decoder = None
        self.columns = None
        self.dtypes = None
        self.discrete_columns = None
        
    def forward(self, x):
        z = self.encoder(x)
        return self.decoder(z)
        
    def fit(self, data: pd.DataFrame, discrete_columns=()) -> "VAE":
        dim_data = data.shape[1]
        self.columns = data.columns
        self.dtypes: pd.Series = data.dtypes
        self.discrete_columns = discrete_columns
        
        self.encoder = VariationalEncoder(
            dim_latent=self.dim_latent,
            dim_data=dim_data,
            dim_hidden=self.dim_hidden,
            activation_fn=F.relu,
        )
        self.decoder = Decoder(
            dim_latent=self.dim_latent,
            dim_data=dim_data,
            dim_hidden=self.dim_hidden,
            activation_fn=F.relu,
        )
        
        data_tensor = _get_tensor_data(data.values)
        return self._train(data_tensor)
    
    def _train(self, data): 
        """Trains model"""
        if self.verbose:
            print("Starting VAE training..")
        self.train()
        
        opt = Adam(self.parameters())
        for epoch in range(self.n_epochs):
            for x in data:
                x = x.to(DEVICE) 
                opt.zero_grad()
                x_hat = self.forward(x)
                loss = ((x - x_hat)**2).sum() + self.encoder.kl
                loss.backward()
                opt.step()
            if self.verbose:
                print(f"complete: {epoch=} {self.encoder.kl=}")
                
        return self
        
    
    def sample(self, n_samples: int) -> pd.DataFrame:
        """Samples new data"""
        self.eval()
        
        values = []
        for _ in range(n_samples): 
            mean, std = torch.zeros(self.dim_latent), torch.ones(self.dim_latent)
            noise = torch.normal(mean=mean, std=std).to(DEVICE)
            sample = self.decoder(noise)
            values.append(sample.detach().numpy())
        
        df = pd.DataFrame(data=values, columns=self.columns)
        # df.loc[:, self.discrete_columns] = df.loc[self.discrete_columns].apply(lambda x: round(x))
        df = df.astype(self.dtypes.to_dict())

        return df

    @classmethod
    def load(cls, path) -> None:
        """Saves the model"""
        model = torch.load(path)
        model.set_device(DEVICE)

        return model

    def save(self, path) -> None:
        """Saves the model"""
        torch.save(self, path)



