\documentclass{article}
\usepackage[utf8]{inputenc}

% BibLaTeX
\usepackage[style=iso-numeric]{biblatex}
\addbibresource{citation.bib}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\graphicspath{ {./img/} }

\title{MVI_checkpoint}
\date{November 2022}
\title{Data augmentation using DL}
\author{Jan Jeníček (jenicja1)}

\begin{document}

\maketitle

\section{Introduction}

Apart from using different methods, the prediction of machine learning tasks can often be significantly improved by using better-quality data. The goal of this semestral work is to use deep learning algorithms to augment the original data set, hopefully lowering the overall prediction error. This work will focus on the augmentation of structured data.

\section{Input data}

For this task, I have chosen a data set from a Kaggle competition that aims to predict prices of realities in Russia \cite{kaggle_dataset}. The dataset consists of information about realities and about macroeconomics at a time of purchase for each 
reality. The dataset is simplified for the purpose of this work and only some of provided features are selected to be used for price prediction.

\section{Goal}

The motivation to augment the dataset comes from the fact that models which were successful in the competition predicted prices separately based on whether an apartment was bought as an investment or as a place for living. Although this improves the overall precision of predictions, it lower the number of samples that can be used to train each of the regression models. This is the case especially for the apartments bought as a place for living, so this work attempts to augment this sub-dataset using DL techniques. 

\section{Approach}

The main idea of the approach main approach is to first preprocess the training set and use it to train a generative DL-based model that learns to sample new data from the training set distribution. The model is used to generate new artificial data that are concatenated with the preprocessed training data, creating an augmented dataset. Both the augmented dataset and the original dataset are finally used to train a regression model for the price prediction task and the results are compared.

\subsection{Preprocessing}

The biggest issue seemed to be the presence of outliers in the dataset. To solve this, apart from other minor preprocessing steps, I removed particular rows based on their \textit{zscore} across all the feature columns. The result can be seen in figures \ref{fig:pca_before} and \ref{fig:pca_after}.

\begin{figure}[h!]
    \begin{center}
        \includegraphics[width=0.4\textwidth]{img/pca_with_outliers.png}
        \caption{Two principal components, before removing the outliers}
        \label{fig:pca_before}
    \end{center}
\end{figure}

\begin{figure}[h!]
    \begin{center}
        \includegraphics[width=0.4\textwidth]{img/pca_without_outliers.png}
        \caption{Two principal components, after removing the outliers}
        \label{fig:pca_after}
    \end{center}
\end{figure}

\clearpage

\subsection{Augmentation}

As the DL generative model, I have chosen two different architectures. First, I trained a \textit{CTGAN} \cite{CTGAN} model which is well suited for tabular data generation as it implements several features that help to solve the challenges that come with structured data in general. I also wanted to compare its performance to a simple DL model, so I implemented vanilla \textit{Variational Autoencoder} \cite{VAE} as it is easy to work with (for example, in comparison to vanilla \textit{GAN} \cite{GAN}). This approach proved to be extremely bad and it failed to generate new samples that would resemble the original ones. Because of that, only data augmented by \textit{CTGAN} were used to train a regression model.

\subsection{Regression}

I used gradient boosting to predict the price. The regression model itself is not fine-tuned, it always uses the same default hyperparameters just to serve as a benchmark.  

I trained 2 models:
\begin{itemize}
    \item \textit{original model} - model that uses the raw training data
    \item \textit{augmented model} - model that uses the augmented training data
\end{itemize}

I evaluated results of both models with RMSE metric and compared the results.

\section{Tech stack}

Both models are implemented using \textit{pytorch} \cite{pytorch}. For the use of \textit{CTGAN}, I leveraged \textit{SDV} library \cite{SDV}. For the regression, I used gradient boosting framework \textit{lightgbm} \cite{lightgbm}.

\section{Results}

Unfortunatelly, I failed to train the vanilla \textit{VAE} to generate meaningful results. For that reason, its output was not used to augment the training dataset. On the other hand, the output of the \textit{CTGAN} model followed the original distribution closely (figure \ref{fig:distr}) and it was used for the augmentation. 

\begin{figure}[h!]
    \begin{center}
        \includegraphics[width=1.0\textwidth]{img/distr_compare.png}
        \caption{Comparison of original and generated distributions}
        \label{fig:distr}
    \end{center}
\end{figure}

With that being said, the predictions of the \textit{augmented model} were worse than the original predictions - the RMSE worsened by 4.42 \%. 
This probably means that the prediction error for this regression task cannot be simply lowered by augmenting the original dataset. With that being said, it might still be a useful technique during following steps, considering we pay more attention to the regression model.   

\clearpage
\printbibliography

\end{document}

